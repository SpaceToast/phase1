# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=elfutils
pkgver=0.176
pkgrel=0
pkgdesc="A collection of utilities and DSOs to handle ELF files and DWARF data"
url="http://elfutils.org/"
arch="all"
license="GPL-3.0-or-later and (GPL-2.0-or-later or LGPL-3.0-or-later)"
depends=""
makedepends="bison flex-dev zlib-dev bzip2-dev xz-dev argp-standalone
	bsd-compat-headers autoconf automake libtool"
install=""
subpackages="$pkgname-dev $pkgname-libelf"
source="https://sourceware.org/elfutils/ftp/$pkgver/elfutils-$pkgver.tar.bz2
	fix-uninitialized.patch
	musl-accessperms.patch
	musl-cdefs.patch
	musl-macros.patch
	musl-qsort_r.patch
	musl-strerror_r.patch
	no-werror.patch
	"
builddir="$srcdir/elfutils-$pkgver"

# secfixes:
#   0.168-r1:
#   - CVE-2017-7607
#   - CVE-2017-7608

prepare() {
	cd "$builddir"
	default_prepare
	autoreconf -vif

	cat > src/error.h <<-EOF
	#ifndef _ERROR_H_
	#define _ERROR_H_

	#include <stdarg.h>
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <errno.h>

	static unsigned int error_message_count = 0;

	static inline void error(int status, int errnum, const char* format, ...)
	{
		va_list ap;
		fprintf(stderr, "%s: ", program_invocation_name);
		va_start(ap, format);
		vfprintf(stderr, format, ap);
		va_end(ap);
		if (errnum)
			fprintf(stderr, ": %s", strerror(errnum));
		fprintf(stderr, "\n");
		error_message_count++;
		if (status)
			exit(status);
	}

	#endif	/* _ERROR_H_ */
EOF
	cp src/error.h lib/ || return 1
	cat > lib/libintl.h <<-EOF
	#ifndef LIBINTL_H
	#define LIBINTL_H
	#define _(x) (x)
	#endif
EOF

}
build() {
	cd "$builddir"
	CC=$CC CXX=$CXX ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-werror \
		--disable-nls \
		--disable-dependency-tracking \
		ac_cv_c99=yes
	cd lib && make -j1
	# for now we only build libelf
	cd ..
	cd libelf && make -j1
}

package() {
	cd "$builddir"/libelf
	make DESTDIR="$pkgdir" install
}

libelf() {
	pkgdesc="libelf from elfutils"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libelf*.so* "$subpkgdir"/usr/lib/
}

sha512sums="7f032913be363a43229ded85d495dcf7542b3c85974aaaba0d984228dc9ac1721da3dc388d3fa02325a80940161db7e9ad2c9e4521a424ad8a7d050c0902915b  elfutils-0.176.tar.bz2
b9ba55e1b56a8abf694b6d02f022d9a3a0ae6ab53a8c4a71e49552e32411ef410d3a7512fbd1a729696bc2046ac11586829816f0fa3b8226ee77c9dc4b741e58  fix-uninitialized.patch
0d9858c8437b5185975bb6be1340bcd718a5e723c7f9d683be91c8b14e87dc9b94fc620d752d07a655c602cc2e35d85d62b829a91d43d930d8f0b3ffde6e0e32  musl-accessperms.patch
0198194976782ab13bba59491d2d6bbe6189cba1edab174ca61cc7b942564f03b5d61feb6b787c5d09e66b3b493f08a330a8612e04de02d261b72b7996cc50c3  musl-cdefs.patch
886ab23301f1872d94bcfc3590621196c811252c9a993738e5e480e666c7c3359f25e94c0e873c8fe16dc283e193dba0532a7ced3951e673185dcbb1d062b7c6  musl-macros.patch
f025d6479c8782275090783ff4dd09eb70a7c3eec1126d3176c02d01124f22864d81e08cb96ac4d255e0316205658459b617f5b661b16dbaf1636591720605f4  musl-qsort_r.patch
a0d986100c8ff2ef0595645ec1b2eeb1d517b7442aef5f349ebf27fcb66c76e51fadeda25bed5f04b4bb16a61aa23ac6e86a1f34a0087d2136acf0f64c3fa4d1  musl-strerror_r.patch
26aff757de8c0c67ae7922888c4a842939fb9b4022f3d2fa916591b44921c109f74154175afd431a0e31cf7c876f6d4fbaae26ef283985ae98d96854ea02dce0  no-werror.patch"
